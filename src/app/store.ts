export const store={
    debug: true,
    state: {
        productos:[]
    },
    addNewProd(newProd){
        let maxId=this.state.productos.reduce((max,item)=>(item.id>max)?item.id:max, 0)
        this.state.productos.push({
            id: maxId+1,
            nombre: newProd.nombre,
            unidades:newProd.unidades,
            precio: newProd.precio
          })
          alert('Producto añadido correctamente');
    },
    delProd(id){
        let index=this.state.productos.findIndex(item=>item.id==id)
        if (index!=-1) {
            this.state.productos.splice(index,1)
        }
    },
    loadAll(response){
        this.state.productos.splice(0,this.state.productos.length);
        response.forEach(element => {
            this.state.productos.push(element);
        });
    }
}