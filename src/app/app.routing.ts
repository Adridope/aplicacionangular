import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {FormularioComponent} from './components/formulario/formulario.component';
import {ProductosComponent} from './components/productos/productos.component';
import {ErrorPageComponent} from './components/error-page/error-page.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'inicio', component: HomeComponent},
    {path: 'productos', component: ProductosComponent},
    {path: 'formulario', component: FormularioComponent},
    {path: '**', component: ErrorPageComponent}
];
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);