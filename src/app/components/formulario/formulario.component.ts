import { Component, OnInit } from '@angular/core';
import axios from '../../axios';
import {store} from '../../store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public producto: any;
  constructor(
    private _router: Router
    ) {
    this.producto = {
      name: '',
      description: '',
      price: ''
    }
   }

  ngOnInit(): void {
  }

  onSubmit(){
    axios.addProd(this.producto)
        .then((response) => {
          store.addNewProd(response.data)
          this._router.navigate(['/productos']);
        })
        .catch((error) =>
          console.error(
            "Error: " + (error.statusText || error.message || error)
          )
        );
        
  }
}
