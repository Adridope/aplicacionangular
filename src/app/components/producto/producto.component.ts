import { Component, Input, OnInit } from '@angular/core';
import axios from '../../axios';
import {store} from '../../store';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  @Input() producto;

  constructor() {}

  ngOnInit(): void {
  }
  borrar(){
    if(confirm(`Vas a borrar el producto "${this.producto.name}"`)){
      axios.deleteProd(this.producto.id)
      .then(() => {
        store.delProd(this.producto.id)
      })
      .catch((error) =>
        console.error(
          "Error: " + (error.statusText || error.message || error)
        )
      );
    }
  
  }
}
