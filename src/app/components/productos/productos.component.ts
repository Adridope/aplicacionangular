import { Component, OnInit } from '@angular/core';
import axios from '../../axios';
import {store} from '../../store';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  public productos: Array<any>;

  constructor() {
    this.productos = store.state.productos;
   }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    axios.getProds()
      .then((response) => {
        store.loadAll(response.data)
      })
      .catch((error) =>
        console.error(
          "Error: " + (error.statusText || error.message || error)
        )
      );
  }
}
