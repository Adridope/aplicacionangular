import axios from 'axios';
const baseURL = 'http://localhost:3000/';

export default {
  getProds() {
    return axios.get(baseURL+'productos')
  },

  addProd(newProd) {
    return axios.post(baseURL+'productos', newProd)
  },
  deleteProd(id){
    return axios.delete(baseURL+'productos/'+ id)
  }
}
