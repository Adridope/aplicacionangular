# AplicacionAngular

## Requisitos previos

**Node**, para instalarlo usaremos:
```
sudo apt install nodejs 
```

El gestor de paquetes de node "**npm**", para instalarlo usaremos: 
```
sudo npm install -g npm@latest 
```

**Angular Cli**, para instalarlo usaremos:
```
npm install -g @angular/cli@latest
```

**Json-server**, para instalarlo usaremos:
```
npm install -g json server
```

## Iniciar el proyecto en modo desarrollo

Una vez todo lo anterior instalado, podemos usar el comando:
```
npm install
```

Para iniciar el proyecto:
```
ng serve 
```

Para iniciar el json-server:
```
json-server productos.json --watch
```
